powershell -Command "& cd ../../.. | west build --pristine -b nice_nano -- -DSHIELD=skeletyl_rene_left"
powershell -Command "& cp ../../../build/zephyr/zmk.uf2 left/zmk.uf2"
powershell -Command "& cd ../../.. | west build --pristine -b nice_nano -- -DSHIELD=skeletyl_rene_right"
powershell -Command "& cp ../../../build/zephyr/zmk.uf2 right/zmk.uf2"